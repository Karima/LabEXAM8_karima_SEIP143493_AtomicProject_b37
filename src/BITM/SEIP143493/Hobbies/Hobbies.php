<?php
namespace App\Hobbies;
use App\Model\Database as DB;

use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Hobbies extends DB{
    public $id="";

    public $name="users";

    public $hobbies="a";



    public function __construct(){

        parent::__construct();

    }
    public function setData($data=NULL){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists('hobbies',$data)){
            $this->hobbies=implode(",",$data['hobbies']);
        }
    }
    public function store(){



        $arrData  = array($this->name,$this->hobbies);

        $sql = "INSERT INTO hobbies ( name, hobbies) VALUES ( ?, ?)";

        $STH = $this->conn->prepare($sql);

        $result=$STH->execute($arrData);

        if($result) {

            Message::message("<div id='msg'></div><h3 align='center'>[ Book Title: $this->name ] , [ Author Name: $this->hobbies ] <br> Data Has Been Inserted Successfully!</h3></div>");

        }
        else{
            Message::message("<div id='msg'></div><h3 align='center'>[ NAME: $this->name ] , [ BIRTH DATE: $this->hobbies ] <br> Data Has not Been Inserted Successfully!</h3></div>");

        }



        Utility::redirect('create.php');

    }





    public function index($fetchMode = "ASSOC")
    {

        $STH = $this->conn->query('SELECT * from hobbies');


        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, "OBJ") > 0) $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;

    }

// end of index()

    public function view($fetchMode = "ASSOC")
    {

        $STH = $this->conn->query('SELECT * from hobbies where id=' . $this->id);

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, "OBJ") > 0) $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData = $STH->fetch();
        return $arrOneData;

    }// end of view()


    public function update(){

        $arrData  = array($this->name,$this->hobbies);

        $sql = 'UPDATE hobbies  SET name = ?   , hobbies= ? where id ='.$this->id;

        $STH = $this->conn->prepare($sql);


        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('index.php');


    }// end of update()


    public function delete(){

        $sql = "DELETE FROM hobbies WHERE id =".$this->id;

        $STH = $this->conn->prepare($sql);

        $result = $STH->execute();

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Deleted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');


    }// end of delete()





}// end of Birthday