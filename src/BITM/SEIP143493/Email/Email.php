<?php
namespace App\Email;
use App\Model\Database as DB;
use PDO;

class Email extends DB{
    public $id="";

    public $name="";

    public $email="";


    public function __construct(){



        parent::__construct();

    }

    public function setData($data=NULL)
    {


        if (array_key_exists('id', $data)) {


            $this->id = $data['id'];
        }

        if (array_key_exists('name', $data)) {


            $this->name= $data['name'];
        }

        if (array_key_exists('email', $data)) {


            $this->email= $data['email'];
        }
    }
    public function store(){



        $sql="insert into email(name,email)values
('$this->name','$this->email')";
        $STH=$this->conn->prepare($sql);

        $STH->execute();








    }

    public function index($fetchMode = "ASSOC")
    {

        $STH = $this->conn->query('SELECT * from email');


        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, "OBJ") > 0) $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;

    }

// end of index()

    public function view($fetchMode = "ASSOC")
    {

        $STH = $this->conn->query('SELECT * from email where id=' . $this->id);

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, "OBJ") > 0) $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData = $STH->fetch();
        return $arrOneData;

    }// end of view()


    public function update(){

        $arrData  = array($this->name,$this->email);

        $sql = 'UPDATE email SET name  = ?   , email = ? where id ='.$this->id;

        $STH = $this->conn->prepare($sql);


        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('index.php');


    }// end of update()


    public function delete(){

        $sql = "DELETE FROM email WHERE id =".$this->id;

        $STH = $this->conn->prepare($sql);

        $result = $STH->execute();

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Deleted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');


    }// end of delete()






}// end of