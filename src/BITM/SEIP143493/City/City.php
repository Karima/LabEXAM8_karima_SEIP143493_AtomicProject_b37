<?php

namespace App\City;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

use PDO;

class City extends DB{

    public $id="";

    public $name="";

    public $city="";
    public $country="";



    public function __construct(){

        parent::__construct();

    }
    public function setData($data=NULL){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists('city',$data)){
            $this->city=$data['city'];
        }
        if(array_key_exists('country',$data)){
            $this->country=$data['country'];
        }
    }
    public function store(){

        $sql="insert into city(name,city,country)values('$this->name','$this->city','$this->country')";

        $STH=$this->conn->prepare($sql);
        $STH->execute();
    }



    public function index($fetchMode = "ASSOC")
    {

        $STH = $this->conn->query('SELECT * from city');


        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, "OBJ") > 0) $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;

    }

// end of index()

    public function view($fetchMode = "ASSOC")
    {

        $STH = $this->conn->query('SELECT * from city where id=' . $this->id);

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, "OBJ") > 0) $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData = $STH->fetch();
        return $arrOneData;

    }// end of view()


    public function update(){

        $arrData  = array($this->name,$this->city,$this->country);

        $sql = 'UPDATE city  SET name  = ?   , city = ?,country=? where id ='.$this->id;

        $STH = $this->conn->prepare($sql);


        $result = $STH->execute($arrData);

        if($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Updated Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Updated Successfully!</h3></div>");

        Utility::redirect('index.php');


    }// end of update()


    public function delete()
    {

        $sql = "DELETE FROM city WHERE id =" . $this->id;

        $STH = $this->conn->prepare($sql);

        $result = $STH->execute();

        if ($result)
            Message::message("<div  id='message'><h3 align='center'> Success! Data Has Been Deleted Successfully!</h3></div>");
        else
            Message::message("<div id='message'><h3 align='center'> Failed! Data Has Not Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');

    }
    }// end of delete()




