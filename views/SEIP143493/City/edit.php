<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\BookTitle\BookTitle;
if(!isset( $_SESSION)) session_start();
echo "<div id =\"message\">". Message::message()."</div>";

$objCity=new City();
$objCity->setData($_GET);
$singleItem=$objCity->view("obj");


?>






<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
    <title>City Edit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Login and Registration Form with HTML5 and CSS3" />
    <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
    <meta name="author" content="Codrops" />
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" type="text/css" href="../../../resource/assets/css/demo2.css" />
    <link rel="stylesheet" type="text/css" href="../../../resource/assets/css/style.css" />
    <link rel="stylesheet" type="text/css" href="../../../resource/assets/css/animate-custom.css" />
</head>
<body>
<div class="container">
    <!-- Codrops top bar -->
    <div class="codrops-top">
        <a href="">
            <strong>&laquo; Previous Demo: </strong>Responsive Content Navigator
        </a>
        <span class="right">
                    <a href=" http://tympanus.net/codrops/2012/03/27/login-and-registration-form-with-html5-and-css3/">
                        <strong>Back to the Codrops Article</strong>
                    </a>
                </span>
        <div class="clr"></div>
    </div><!--/ Codrops top bar -->
    <header>
        <h1>ATOMIC PROJECT <span>City Edit</span></h1>
        <nav class="codrops-demos">
            <span>Click <strong>"Join us"</strong> to see the form switch</span>
            <a href="index.html" class="current-demo">Demo 1</a>
            <a href="index2.html">Demo 2</a>
            <a href="index3.html">Demo 3</a>
        </nav>
    </header>
    <section>
        <div id="container_demo" >
            <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
            <a class="hiddenanchor" id="toregister"></a>
            <a class="hiddenanchor" id="tologin"></a>
            <div id="wrapper">


                <div id="login" class="animate form">







                    <form role="form" action="update.php" method="post" class="login-form">
                        <input hidden name="id" value="<?php echo $singleItem->id?>">

                        <h1>City</h1>
                        <div class="form-group">
                            <label>Name</label>

                            <input type="text" name="name" value ="<?php echo $singleItem->name ?>"class="form-username form-control" id="form-username">
                        </div>
                        <div class="form-group">
                            <label>City Name</label>

                            <input list="browsers" name="city" value ="<?php echo $singleItem->city ?>" class="form-username form-control" id="form-username">
                            <datalist id="browsers">
                                <option value="Chittagong">
                                <option value="Dhaka">
                                <option value="Joshor">
                                <option value="Kummillah">
                                <option value="Ptuakali">
                                <option value="chandpur">
                            </datalist>
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-book_title"></label>
                            <input list="browsers1" name="country" value ="<?php echo $singleItem->country ?>" class="form-username form-control" id="form-username">
                            <datalist id="browsers1">
                                <option value="Bangladesh">
                                <option value="India">
                                <option value="Pakistan">
                                <option value="America">
                                <option value="Uganda">
                                <option value="Srilanka">
                            </datalist>
                        </div>

                        <p class="login button">
                            <input type="submit" value="Create" />
                        </p>
                        <p class="change_link">
                            <a href="#toregister" class="to_register">Join us</a>
                        </p>

                    </form>





                </div>









            </div>
        </div>
    </section>
</div>
</body>
</html>




<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>
