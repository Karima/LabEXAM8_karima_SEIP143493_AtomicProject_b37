<?php
require_once("../../../vendor/autoload.php");
use App\Database;
use App\Hobbies;
use App\Message\Message;


$book=new \App\Hobbies\Hobbies();
echo $book->index();
$AllData= $book->index();
if(!isset( $_SESSION)) session_start();
echo "<div id =\"message\">".Message::message()."</div>";
?>




<html>
<head>

    <meta charset="UTF-8" />
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  -->
    <title>City</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Login and Registration Form with HTML5 and CSS3" />
    <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
    <meta name="author" content="Codrops" />
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" type="text/css" href="../../../resource/assets/css/demo2.css" />
    <link rel="stylesheet" type="text/css" href="../../../resource/assets/css/style.css" />
    <link rel="stylesheet" type="text/css" href="../../../resource/assets/css/animate-custom.css" />
</head>
<body>

<table style="width:100%">
      <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Hobbies</th>
        <th>Button</th>
          </tr>

    <?php foreach($AllData as $data) { ?>
        <tr>
            <td><?php echo $data['id']  ?></td>
            <td><?php echo $data['name']?></td>
            <td><?php echo $data[hobbies]?></td>
            <td><a href="edit.php?id=<?php echo $data['id']  ?>"><button>Edit</button><a></a>
                    <a href="delete.php?id=<?php echo $data['id']  ?>"><button>Delete</button><a></a></td>
        </tr>

    <?php }?>

</table>

</body>
</html>
<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>